var app = require('express')();
var http = require('http').Server(app)
var io = require('socket.io')(http);
var cmd=require('node-cmd')
var macAddr = require('node-getmac')
var resourceGroup = "buffalo"
var vms = ['buffaload', 'apache', 'squidproxy', 'sessionhost1']
var stoppedVms = []
var sessionHost = 'SessionHost1'
var sessionhostIP = 'SessionHost1-ip'
var MongoClient = require('mongodb').MongoClient
var assert = require('assert');
var mongoUrl = 'mongodb://buffalo:p8pRt4jYLmdrfF8UDYYPW3Liok4pZZbCAcoKUMq5OYRAlq6sSZ7PRHqiYpPX8llet6cPqxpCD30ZzGJgB8jKBQ==@buffalo.documents.azure.com:10250/?ssl=true';

// Setup Loading Page Server
app.get('/', function(req,res) {
	res.sendFile(__dirname + '/index.html');
});

http.listen(3002, function() {
	console.log('listening on *:3002')
})

// Start Chrome - WILL BE DIFFERENT IN LINUX
cmd.run('chromium-browser http://localhost:3002/')


function mongoAdd(url) {
	// Connection URL 
	// Use connect method to connect to the Server 
	MongoClient.connect(url, function(err, db) {
		io.emit('debug', 'Connected to Mongo DB')
  		assert.equal(null, err);
  		var collection = db.collection('loggedinpcs');
  		collection.insert({macAddr:macAddr}) 
  		io.emit('azure', "Registering your Mac Address " + macAddr)
  		db.close();
	});

}

function mongoRemove(url) {
	MongoClient.connect(url, function(err, db) {
		io.emit('debug', 'Connected to MongoDB')
	  	assert.equal(null, err);
  		console.log("Connected correctly to server");
  		var collection = db.collection('loggedinpcs');
  		collection.deleteOne({macAddr:macAddr})
  		io.emit('azure', 'Removed '+macAddr+' from DB')
  		collection.find({}) .toArray(function(err,docs) {
			io.emit('debug', 'Found '+docs)
  			if(docs.length == 0) {
  				// Everyone has logged off, Shutdown.
		  		io.emit('azure', 'Everyone has logged off, Shutting Down Azure Instrastructure')
				shutDown()  		
  			}
  		})
  		db.close();
	});
}

function shutDown() {
	io.emit('azure', 'You were the last person connected, please wait while we shut down the cloud infrastructure')
	io.emit('loadingbar', 10)
	var count = 0
	for (var vm in vms) {
		cmd.get('azure vm deallocate ' + resourceGroup + ' ' + vms[vm], function(data) {
			io.emit('debug', data)
			count++
			console.log(data)
			if (count==vms.length) {
				io.emit('azure', 'All Done :) You can Now Shutdown')
			}
		})
	}
}


function connectToSessionHost(sessionhostIP, url) {
	cmd.get('azure network public-ip show buffalo '+sessionhostIP+' --json', function(data) {
		io.emit('debug', data)
		var data = JSON.parse(data)
		console.log(data.ipAddress)
			io.emit('azure', 'Launching RDP Client')
			cmd.get('xfreerdp /v:'+data.ipAddress+' /cert-ignore /sec:tls /sound:sys:alsa /multimedia:sys:alsa /f', function(data) {
				//console.log(data)
				io.emit('debug', data)
				io.emit('loading', "Logging Off")
				//console.log('user has exited the TS Session')
				io.emit('status', 'Performing Log-Off Actions')
				io.emit('azure', 'Logging You Off From Cloud Services')
				mongoRemove(url)
			//	// Run Shutdown Script
			//	//cmd.get('node azurestop.js', function(data) {
			//	//	console.log(data)
				//})
		})
	})
}


//Login to Azure 
io.emit('status', 'Setting Up Cloud Connection')
cmd.get('azure login --service-principal --tenant 9f5fcb6c-94b7-4b82-a61e-32fc334aef53 -u 8115a9d5-ddcf-460b-b7c8-7773ed6ff88b --certificate-file ~/Downloads/buffalocertificate.pem --thumbprint 07E2D519885C09D67510A63BFAB74F8A05985946', function(data) {
	io.emit('azure', 'Succesfully Connected To Azure')
	cmd.get('azure vm list buffalo --json', function(data) {
			io.emit('azure', 'Retreived List of VMs')
			io.emit('debug', data)
 			var data = JSON.parse(data)
 			for (var vm in data) {
 				var powerState = data[vm].powerState
 				if (powerState != "VM running") {
 					stoppedVms.push(data[vm].name)
 				}
 			}
 			if (stoppedVms.length !=0) {
 				io.emit('status', "Azure Infrastrucure Not Running, Machines Are Being Started. This may take up to 5 minutes, see below for updates")
 				io.emit('debug', 'Stopped VMS ' + stoppedVms)
 				var count = 0
 				var loading = 30
 				for (var vm in stoppedVms) {
					cmd.get('azure vm start ' + resourceGroup + ' ' + stoppedVms[vm], function(data) {
						io.emit('azure', "Started " + stoppedVms[count])
						io.emit('debug', data)
						count++
						if (count == stoppedVms.length) {
							io.emit('status', "Connecting You")
							io.emit('azure', '')
							// Add Mac to MongoDB
							mongoAdd(mongoUrl);
							// Set 30 second wait for machine to actually boot
							setTimeout(function() {connectToSessionHost(sessionhostIP, mongoUrl)}, 30000);
						}
					})
				}	
			}
			else {
				io.emit('status', "Connecting You")
				io.emit('azure', '')
				io.emit('debug', 'Azure Cloud Already Running')
				mongoAdd(mongoUrl);
				connectToSessionHost(sessionhostIP, mongoUrl)
			}	
 	})	
})
